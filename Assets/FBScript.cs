﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Facebook.Unity;

public class FBScript : MonoBehaviour {

	public GameObject DialogLoggedIn;
	public GameObject DialogLoggedOut;
	public GameObject DialogUserName;
	public GameObject DialogProfilePic;
	public GameObject DialogEmail;
    public GameObject DialogLinkedIn;
    public GameObject DialogGooglePlus;

	void Awake() {
		// Create an instance of facebook
		FacebookManager.Instance.InitFB();
		DealWithFBMenus(FB.IsLoggedIn);
	}

	public void FBlogin() {

		List<string> permissions = new List<string> ();
		permissions.Add("public_profile");

		// Once got the permission, call AuthCallBack
		FB.LogInWithReadPermissions(permissions, AuthCallBack);
	}

	void AuthCallBack(IResult result) {

		if(result.Error != null) {
			Debug.Log(result.Error);
		} else {
			if (FB.IsLoggedIn) {
				FacebookManager.Instance.IsLoggedIn = true;
				FacebookManager.Instance.GetProfile();
				Debug.Log("FB is logged in");
			} else {
				Debug.Log("FB is not logged in");
			}

			DealWithFBMenus(FB.IsLoggedIn);
		}
	}

	//Showing and hiding log in menu
	void DealWithFBMenus(bool isLoggedIn) {
		
		if (isLoggedIn) {
			DialogLoggedIn.SetActive(true);
            DialogGooglePlus.SetActive(false);
            DialogLinkedIn.SetActive(false);
            DialogLoggedOut.SetActive(false);

			// When it set the name
			if (FacebookManager.Instance.ProfileName != null) {
				Text UserName = DialogUserName.GetComponent<Text>();
				Text UserEmail = DialogEmail.GetComponent<Text>();
				UserName.text = "" + FacebookManager.Instance.ProfileName;
				UserEmail.text = "" + FacebookManager.Instance.ProfileEmail;

			// When there is no name
			} else {
				// Check gathering profilename
				StartCoroutine ("WaitForProfileName");
			}

			// When it set the profilepic
			if (FacebookManager.Instance.ProfileName != null) {
				Image ProfilePic = DialogProfilePic.GetComponent<Image>();
				ProfilePic.sprite = FacebookManager.Instance.ProfilePic;

			// When there is no pic
			} else {
				// Check gathering profilepic
				StartCoroutine ("WaitForProfilePic");
			}

		} else {
			DialogLoggedIn.SetActive(false);
			DialogLoggedOut.SetActive(true);
		}
	}

	// Check gathering profilename from the server
	IEnumerator WaitForProfileName()
	{
		while (FacebookManager.Instance.ProfileName == null) {
			// Wait from frame
			yield return null;
		}

		DealWithFBMenus (FacebookManager.Instance.IsLoggedIn);
	}

	// Check gathering profilepic from the server
	IEnumerator WaitForProfilePic()
	{
		while (FacebookManager.Instance.ProfilePic == null) {
			// Wait from frame
			yield return null;
		}

		DealWithFBMenus (FacebookManager.Instance.IsLoggedIn);
	}

	public void Share()
	{
		FacebookManager.Instance.Share();
	}

	public void Invite()
	{
		FacebookManager.Instance.Invite();
	}
}
