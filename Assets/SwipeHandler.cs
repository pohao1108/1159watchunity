﻿using UnityEngine;
using UnityEngine.UI;

// This script will tell you which direction you swiped in
public class SwipeHandler : MonoBehaviour
{
	[Tooltip("The text element we will display the swipe information in")]
	public Text InfoText;

	//Assign menu btn to trigger slide menu
	public GameObject mMenuBtn;
	public GameObject panel;
	private Animator animator;

	// Color Panel
	public GameObject colorPanel;
	public GameObject colorBtnOn;
	public GameObject colorBtnOff;
	private Animator colorAnimator;

	// For modification
	private GameObject watchGeneratorUI;
	private GameObject camera;

	// RayCasting
	Ray ray;
	Renderer selectedRenderer;

	protected virtual void OnEnable()
	{
		// Hook into the OnSwipe event
		Lean.LeanTouch.OnFingerSwipe += OnFingerSwipe;
	}

	protected virtual void OnDisable()
	{
		// Unhook from the OnSwipe event
		Lean.LeanTouch.OnFingerSwipe -= OnFingerSwipe;
	}

	public void OnFingerSwipe(Lean.LeanFinger finger)
	{
		// Make sure the info text exists
		if (InfoText != null)
		{
			// Store the swipe delta in a temp variable
			var swipe = finger.SwipeDelta;
			var left  = new Vector2(-1.0f,  0.0f);
			var right = new Vector2( 1.0f,  0.0f);
			var down  = new Vector2( 0.0f, -1.0f);
			var up    = new Vector2( 0.0f,  1.0f);

			if (SwipedInThisDirection(swipe, left) == true)
			{
				// If the menu is on and swipe left then turn off the menu 22.09.16 Dongyeop
				// Find the close btn
				panel = GameObject.FindGameObjectWithTag("MainMenu");
				animator = panel.GetComponent<Animator>();

				/*
					Turn on/off like up/down ?? 22.09 Dongyeop
				*/
				// Call closing animation
				MainMenuBtn menuBtn = mMenuBtn.GetComponent<MainMenuBtn>();
				menuBtn.DisableBoolAnimator(animator);
				Debug.Log("Swiped to close menu");
				InfoText.text = "You swiped left!";
			}

			if (SwipedInThisDirection(swipe, right) == true)
			{
				InfoText.text = "You swiped right!";
			}

			if (SwipedInThisDirection(swipe, down) == true)
			{
				//Find the colorpanel canvas
				colorPanel = GameObject.FindGameObjectWithTag("ColorPanel");
				colorAnimator = colorPanel.GetComponent<Animator>();

				// Call Slide Up animation
				colorAnimator.SetBool("SlideDown", true);
				colorAnimator.SetBool("SlideUp", false);
				InfoText.text = "You swiped down!";
			}

			if (SwipedInThisDirection(swipe, up) == true)
			{
				//Find the colorpanel canvas
				colorPanel = GameObject.FindGameObjectWithTag("ColorPanel");
				colorAnimator = colorPanel.GetComponent<Animator>();

				// Call Slide Up animation
				colorAnimator.SetBool("SlideUp", true);
				colorAnimator.SetBool("SlideDown", false);
				InfoText.text = "You swiped up!";
			}

			if (SwipedInThisDirection(swipe, left + up) == true)
			{
				InfoText.text = "You swiped left and up!";
			}

			if (SwipedInThisDirection(swipe, left + down) == true)
			{
				InfoText.text = "You swiped left and down!";
			}

			if (SwipedInThisDirection(swipe, right + up) == true)
			{
				InfoText.text = "You swiped right and up!";
			}

			if (SwipedInThisDirection(swipe, right + down) == true)
			{
				InfoText.text = "You swiped right and down!";
			}
		}
	}

	/*
     * 20.10.2016 Dongyeop
     * This function is to turn on the color panel with the button
     */
	public void ColorMenuOn()
	{
		//Find the colorpanel canvas
		colorPanel = GameObject.FindGameObjectWithTag("ColorPanel");
		colorAnimator = colorPanel.GetComponent<Animator>();

		// Call Slide Up animation
		colorAnimator.SetBool("SlideUp", true);
		colorAnimator.SetBool("SlideDown", false);

		//Switch the buttons
		//colorBtnOn.SetActive(false);
		//colorBtnOff.SetActive(true);

		InfoText.text = "Color Menu On";
	}

	/*
     * 20.10.2016
     * This function is to Turn off the clor panel with the button
     */
	public void ColorMenuOff()
	{
		//Find the colorpanel canvas
		colorAnimator = colorPanel.GetComponent<Animator>();

		// Call Slide Up animation
		colorAnimator.SetBool("SlideDown", true);
		colorAnimator.SetBool("SlideUp", false);

		//Switch the buttons
		//colorBtnOff.SetActive(false);
		//colorBtnOn.SetActive(true);

		InfoText.text = "Color Menu Off";
	}

	private bool SwipedInThisDirection(Vector2 swipe, Vector2 direction)
	{
		// Find the normalized dot product between the swipe and our desired angle (this will return the acos between the vectors)
		var dot = Vector2.Dot(swipe.normalized, direction.normalized);

		// With 8 directions, each direction takes up 45 degrees (360/8), but we're comparing against dot product, so we need to halve it
		var limit = Mathf.Cos(22.5f * Mathf.Deg2Rad);

		// Return true if this swipe is within the limit of this direction
		return dot >= limit;
	}



}