﻿using UnityEngine;
using System.Collections;

public class RotationObject : MonoBehaviour {

	public GameObject object1;
	public GameObject object2;
	public GameObject object3;

	public int Speed = 15;
	public int SpeedDown =1;
	
	// Update is called once per frame
	void Update () {
		object1.transform.Rotate (Vector3.up * Time.deltaTime * Speed, Space.World);
		object2.transform.Rotate (Vector3.up * Time.deltaTime * Speed, Space.World);
		object3.transform.Rotate (Vector3.up * Time.deltaTime * Speed, Space.World);


		//object1.transform.Rotate (Vector3.down * Time.deltaTime * SpeedDown);
		//object2.transform.Rotate (Vector3.down * Time.deltaTime * SpeedDown);
		//object3.transform.Rotate (Vector3.down * Time.deltaTime * SpeedDown);

	}
}
