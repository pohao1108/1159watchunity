﻿using UnityEngine;
using System.Collections;
using AssemblyCSharp;

public class CameraControl : MonoBehaviour {

	public MainMenuBtn mMainMenu;
	public View CameraView { get; set; }

	private float speed = 12.0f;  // Degrees per second;
	public Vector3 euler = Vector3.zero;
	public float xLimit = 80.0f;

	[Tooltip("The minimum field of view angle we want to zoom to")]
	public float Minimum = 50.0f;

	[Tooltip("The maximum field of view angle we want to zoom to")]
	public float Maximum = 160.0f;

	void Start() {
		transform.eulerAngles = euler;
	}
	void Update () {
		if (Lean.LeanTouch.Fingers.Count == 1 && mMainMenu.active == false) {
			float deltax = Lean.LeanTouch.DragDelta.x;
			float deltay = Lean.LeanTouch.DragDelta.y;

			euler.x -= deltay * speed * Time.deltaTime;
			euler.y += deltax * speed * Time.deltaTime;
			euler.x = Mathf.Clamp (euler.x, -xLimit, xLimit);

			transform.eulerAngles = euler;
//			Debug.Log (transform.eulerAngles.ToString());
			float y_axis = transform.eulerAngles.y;
			if (y_axis < 60 || y_axis > 300) {
//				Debug.Log ("Front");
				CameraView = View.FRONT;
			} else if (y_axis > 120 && y_axis < 240) {
//				Debug.Log ("Back");
				CameraView = View.BACK;
			} else {
//				Debug.Log ("Side");
				CameraView = View.SIDE;
			}
		}
	}
}
