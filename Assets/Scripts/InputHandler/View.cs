﻿using System;

namespace AssemblyCSharp
{
	public enum View
	{
		FRONT,
		SIDE,
		ANGLE,
		BACK
	}
}

