﻿using UnityEngine;
using System.Collections;

public class SelectTemplate : MonoBehaviour {
	Ray ray;

	void Start () {
		
	}
	// Update is called once per frame
	void Update () {
		if (Input.GetMouseButton (0)) {
			CastRay ();
		} 
	}

	public void CastRay() {
		Ray toMouse = Camera.main.ScreenPointToRay(Input.mousePosition);
		RaycastHit rhInfo;
		//Raycast return true or false
		bool didHit = Physics.Raycast(toMouse, out rhInfo, 1500.0f);
		if(didHit) {
			Debug.Log(rhInfo.collider.name + "  " + rhInfo.point + "Tag: " + rhInfo.collider.tag + "Object: " + rhInfo.collider.gameObject);
			Debug.DrawRay(ray.origin, ray.direction * 20, Color.red);

			if(rhInfo.collider.tag == "CircleWatch") {
				Application.LoadLevel ("WatchGeneratorNexus");
			}
		} else {
			Debug.Log("Not hit");
		}
	}
}
