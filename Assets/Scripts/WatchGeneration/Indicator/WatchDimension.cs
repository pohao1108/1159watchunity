﻿using System;

namespace AssemblyCSharp
{
	public enum WatchDimension
	{
		HEIGHT,
		OUTRADIUS,
		INRADIUS,
		NONE
	}
}

