﻿using UnityEngine;
using System.Collections;
using AssemblyCSharp;

public class BezelHeight : IndicatorBehaviour{
	public WatchBezel bezel;
	float offset = .008f;

	// Use this for initialization
	void Start () {
		viewDetector = center.GetComponent<CameraControl> ();

		filter = GetComponent<MeshFilter> ();
		render = GetComponent<MeshRenderer> ();
		render.material.shader = Shader.Find ("Sprites/Default");
		target = WatchDimension.HEIGHT;

		Generate ();
		init ();
	}
	
	// Update is called once per frame
	void Update () {
		if (viewDetector.CameraView == View.SIDE && active && !bezel.scaling)
			render.enabled = true;
		else if (bezel.scaling)
			render.enabled = false;
		else {
			render.enabled = false;
			Unselected ();
		}
	}

	public override void Generate() {
		indicator = new Indicator (new Vector3 (bezel.caseScript.OutRadius + offset, bezel.caseScript.Height/2 + bezel.Height, 0),
			new Vector3 (0, -1, 0), new Vector3 (0, 0, 1), bezel.Height);
	}
}
