﻿using UnityEngine;
using System.Collections;
using AssemblyCSharp;

public class BezelRadius : IndicatorBehaviour {
	public WatchBezel bezel;
	float offset = .002f;

	// Use this for initialization
	void Start () {
		viewDetector = center.GetComponent<CameraControl> ();

		filter = GetComponent<MeshFilter> ();
		render = GetComponent<MeshRenderer> ();
		render.material.shader = Shader.Find ("Sprites/Default");
		target = WatchDimension.OUTRADIUS;

		Generate ();
		init ();
	}
	
	// Update is called once per frame
	void Update () {
		if (viewDetector.CameraView == View.FRONT && active && !bezel.scaling)
			render.enabled = true;
		else if (bezel.scaling)
			render.enabled = false;
		else {
			render.enabled = false;
			Unselected ();
		}
	}

	public override void Generate() {
		indicator = new Indicator(new Vector3 (bezel.BottomOutRadius/1.4f, bezel.Height + bezel.caseScript.Height, -bezel.BottomOutRadius/1.4f),
			new Vector3(-1/1.4f, 0, 1/1.4f), new Vector3(1/1.4f, 0, 1/1.4f), bezel.BottomOutRadius - bezel.BottomInRadius + offset);
	}
}
