﻿using System;
using UnityEngine;
using ProceduralToolkit;

namespace AssemblyCSharp
{
	public class Indicator
	{
		// the positive side top vertices
		Vector3 top;
		Vector3 orientation;
		Vector3 platVector;
		MeshDraft draft;
		float scale = 0.04f;
		float distance;
//		Color active = Color.yellow;
//		Color inactive = Color.gray;

		//				 ^ orientation 
		//				 |

		//			vertices[0]
		//			     /\
		//			    /  \
		//			   /    \
		//			  /______\     ---> platVector
		//			[1]      [2]

		public Indicator (Vector3 top, Vector3 orientation, Vector3 platVector, float distance)
		{
			this.top = top;
			this.orientation = orientation;
			this.platVector = platVector;
			this.distance = distance;
			PopulateTriangle ();
		}

		private void PopulateTriangle() {

			// vertical distance
			float ver = scale * 1.7f;
			// half horizontal distance
			float hor = scale;
			Vector3 tr1 = new Vector3 (top.x - orientation.x * ver + platVector.x * hor, 
				top.y - orientation.y * ver + platVector.y * hor, 
				top.z - orientation.z * ver + platVector.z * hor);
			Vector3 tr2 = new Vector3 (top.x - orientation.x * ver - platVector.x * hor, 
				top.y - orientation.y * ver - platVector.y * hor, 
				top.z - orientation.z * ver - platVector.z * hor); 
//			draft = MeshDraft.Indicator (top, tr1, tr2);
			draft = MeshDraft.Indicator(top, tr1, tr2);

			Vector3 top2 = top + orientation * distance;
			Vector3 tr3 = new Vector3 (top2.x + orientation.x * ver + platVector.x * hor,
				              top2.y + orientation.y * ver + platVector.y * hor,
				              top2.z + orientation.z * ver + platVector.z * hor);
			Vector3 tr4 = new Vector3 (top2.x + orientation.x * ver - platVector.x * hor, 
				top2.y + orientation.y * ver - platVector.y * hor, 
				top2.z + orientation.z * ver - platVector.z * hor); 
			var draft2 = MeshDraft.Indicator (top2, tr3, tr4);

			draft.Add (draft2);
		}

		public Mesh ToMesh() {
			return draft.ToMesh ();
		}
	}
}

