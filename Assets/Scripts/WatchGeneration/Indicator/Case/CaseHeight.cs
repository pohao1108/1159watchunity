﻿using UnityEngine;
using System.Collections;
using AssemblyCSharp;

public class CaseHeight : IndicatorBehaviour {
	public WatchCase case_;
	float offset = .002f;
	// Use this for initialization
	void Start () {
		viewDetector = center.GetComponent<CameraControl> ();

		filter = GetComponent<MeshFilter> ();
		render = GetComponent<MeshRenderer> ();
		render.material.shader = Shader.Find ("Sprites/Default");
		target = WatchDimension.HEIGHT;

		Generate ();
		init ();
	}

	// Update is called once per frame
	void Update () {
		if (viewDetector.CameraView == View.SIDE && active && !case_.scaling)
			render.enabled = true;
		else if (case_.scaling)
			render.enabled = false;
		else {
			render.enabled = false;
			Unselected ();
		}
	}

	public override void Generate ()
	{
		indicator = new Indicator(new Vector3(case_.OutRadius + offset, case_.Height / 2, 0),
			new Vector3(0, -1, 0), new Vector3(0, 0, 1), case_.Height);
	}
}
