﻿using UnityEngine;
using System.Collections;
using AssemblyCSharp;

public class CaseRadius : IndicatorBehaviour {
	public WatchCase case_;
	float offset = .002f;
	// Use this for initialization
	void Start () {
		viewDetector = center.GetComponent<CameraControl> ();

		filter = GetComponent<MeshFilter> ();
		render = GetComponent<MeshRenderer> ();
		render.material.shader = Shader.Find ("Sprites/Default");
		target = WatchDimension.OUTRADIUS;

		Generate ();

		init ();
	}
	
	// Update is called once per frame
	void Update () {
		if (viewDetector.CameraView == View.FRONT && active && !case_.scaling)
			render.enabled = true;
		else if (case_.scaling)
			render.enabled = false;
		else {
			render.enabled = false;
			Unselected ();
		}
	}

	public override void Generate ()
	{
		indicator = new Indicator(new Vector3(case_.OutRadius / 1.4f, case_.Height, case_.OutRadius / 1.4f),
			new Vector3(-1/1.4f, 0, -1/1.4f), new Vector3(-1/1.4f, 0, 1/1.4f), case_.OutRadius-case_.InRadius);
	}
}
