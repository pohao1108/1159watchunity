﻿using UnityEngine;
using System.Collections;
using AssemblyCSharp;

public class CaseVertical : IndicatorBehaviour {
	/**
	 * all vector convention using x z y since we rotate the gameobject 
	 **/
	public WatchCase case_;
	float offset = .002f;

	// Use this for initialization
	void Start () {
		viewDetector = center.GetComponent<CameraControl> ();

		filter = GetComponent<MeshFilter> ();
		render = GetComponent<MeshRenderer> ();
		render.material.shader = Shader.Find ("Sprites/Default");
		target = WatchDimension.NONE;

		Generate ();

		init ();
	}
	
	// Update is called once per frame
	void Update () {
		if (viewDetector.CameraView == View.FRONT && active && !case_.scaling)
			render.enabled = true;
		else if (case_.scaling)
			render.enabled = false;
		else {
			render.enabled = false;
			Unselected ();
		}
	}

	public override void Generate ()
	{
		indicator = new Indicator(new Vector3(0, case_.Height, -case_.OutRadius + offset),
			new Vector3(0, 0, 1), new Vector3(1, 0, 0), case_.OutRadius-case_.InRadius);
	}
}
