﻿using System.Collections.Generic;
using UnityEngine;

namespace ProceduralToolkit
{
	public partial class MeshDraft
	{
		#region Watch Case
		public static MeshDraft WatchCase(float height, int nbSides, float bottomRadius1, float bottomRadius2, float topRadius1, float topRadius2)
		{
			int nbVerticesCap = nbSides * 2 + 2;
			int nbVerticesSides = nbSides * 2 + 2;

			#region Vertices

			// bottom + top + sides
			Vector3[] vertices = new Vector3[nbVerticesCap * 2 + nbVerticesSides * 2];
			int vert = 0;
			float _2pi = Mathf.PI * 2f;

			// Bottom cap
			int sideCounter = 0;
			while( vert < nbVerticesCap )
			{
				sideCounter = sideCounter == nbSides ? 0 : sideCounter;

				float r1 = (float)(sideCounter++) / nbSides * _2pi;
				float cos = Mathf.Cos(r1);
				float sin = Mathf.Sin(r1);
				vertices[vert] = new Vector3( cos * (bottomRadius1 - bottomRadius2 * .5f), -height/2, sin * (bottomRadius1 - bottomRadius2 * .5f));
				vertices[vert+1] = new Vector3( cos * (bottomRadius1 + bottomRadius2 * .5f), -height/2, sin * (bottomRadius1 + bottomRadius2 * .5f));
				vert += 2;
			}

			// Top cap
			sideCounter = 0;
			while( vert < nbVerticesCap * 2 )
			{
				sideCounter = sideCounter == nbSides ? 0 : sideCounter;

				float r1 = (float)(sideCounter++) / nbSides * _2pi;
				float cos = Mathf.Cos(r1);
				float sin = Mathf.Sin(r1);
				vertices[vert] = new Vector3( cos * (topRadius1 - topRadius2 * .5f), height/2, sin * (topRadius1 - topRadius2 * .5f));
				vertices[vert+1] = new Vector3( cos * (topRadius1 + topRadius2 * .5f), height/2, sin * (topRadius1 + topRadius2 * .5f));
				vert += 2;
			}

			// Sides (out)
			sideCounter = 0;
			while (vert < nbVerticesCap * 2 + nbVerticesSides )
			{
				sideCounter = sideCounter == nbSides ? 0 : sideCounter;

				float r1 = (float)(sideCounter++) / nbSides * _2pi;
				float cos = Mathf.Cos(r1);
				float sin = Mathf.Sin(r1);

				vertices[vert] = new Vector3(cos * (topRadius1 + topRadius2 * .5f), height/2, sin * (topRadius1 + topRadius2 * .5f));
				vertices[vert + 1] = new Vector3(cos * (bottomRadius1 + bottomRadius2 * .5f), -height/2, sin * (bottomRadius1 + bottomRadius2 * .5f));
				vert+=2;
			}

			// Sides (in)
			sideCounter = 0;
			while (vert < vertices.Length )
			{
				sideCounter = sideCounter == nbSides ? 0 : sideCounter;

				float r1 = (float)(sideCounter++) / nbSides * _2pi;
				float cos = Mathf.Cos(r1);
				float sin = Mathf.Sin(r1);

				vertices[vert] = new Vector3(cos * (topRadius1 - topRadius2 * .5f), height/2, sin * (topRadius1 - topRadius2 * .5f));
				vertices[vert + 1] = new Vector3(cos * (bottomRadius1 - bottomRadius2 * .5f), -height/2, sin * (bottomRadius1 - bottomRadius2 * .5f));
				vert += 2;
			}
			#endregion

			#region Normales

			// bottom + top + sides
			Vector3[] normales = new Vector3[vertices.Length];
			vert = 0;

			// Bottom cap
			while( vert < nbVerticesCap )
			{
				normales[vert++] = Vector3.down;
			}

			// Top cap
			while( vert < nbVerticesCap * 2 )
			{
				normales[vert++] = Vector3.up;
			}

			// Sides (out)
			sideCounter = 0;
			while (vert < nbVerticesCap * 2 + nbVerticesSides )
			{
				sideCounter = sideCounter == nbSides ? 0 : sideCounter;

				float r1 = (float)(sideCounter++) / nbSides * _2pi;

				normales[vert] = new Vector3(Mathf.Cos(r1), 0f, Mathf.Sin(r1));
				normales[vert+1] = normales[vert];
				vert+=2;
			}

			// Sides (in)
			sideCounter = 0;
			while (vert < vertices.Length )
			{
				sideCounter = sideCounter == nbSides ? 0 : sideCounter;

				float r1 = (float)(sideCounter++) / nbSides * _2pi;

				normales[vert] = -(new Vector3(Mathf.Cos(r1), 0f, Mathf.Sin(r1)));
				normales[vert+1] = normales[vert];
				vert+=2;
			}
			#endregion

			#region UVs
			Vector2[] uvs = new Vector2[vertices.Length];

			vert = 0;
			// Bottom cap
			sideCounter = 0;
			while( vert < nbVerticesCap )
			{
				float t = (float)(sideCounter++) / nbSides;
				uvs[ vert++ ] = new Vector2( 0f, t );
				uvs[ vert++ ] = new Vector2( 1f, t );
			}

			// Top cap
			sideCounter = 0;
			while( vert < nbVerticesCap * 2 )
			{
				float t = (float)(sideCounter++) / nbSides;
				uvs[ vert++ ] = new Vector2( 0f, t );
				uvs[ vert++ ] = new Vector2( 1f, t );
			}

			// Sides (out)
			sideCounter = 0;
			while (vert < nbVerticesCap * 2 + nbVerticesSides )
			{
				float t = (float)(sideCounter++) / nbSides;
				uvs[ vert++ ] = new Vector2( t, 0f );
				uvs[ vert++ ] = new Vector2( t, 1f );
			}

			// Sides (in)
			sideCounter = 0;
			while (vert < vertices.Length )
			{
				float t = (float)(sideCounter++) / nbSides;
				uvs[ vert++ ] = new Vector2( t, 0f );
				uvs[ vert++ ] = new Vector2( t, 1f );
			}
			#endregion

			#region Triangles
			int nbFace = nbSides * 4;
			int nbTriangles = nbFace * 2;
			int nbIndexes = nbTriangles * 3;
			int[] triangles = new int[nbIndexes];

			// Bottom cap
			int i = 0;
			sideCounter = 0;
			while (sideCounter < nbSides)
			{
				int current = sideCounter * 2;
				int next = sideCounter * 2 + 2;

				triangles[ i++ ] = next + 1;
				triangles[ i++ ] = next;
				triangles[ i++ ] = current;

				triangles[ i++ ] = current + 1;
				triangles[ i++ ] = next + 1;
				triangles[ i++ ] = current;

				sideCounter++;
			}

			// Top cap
			while (sideCounter < nbSides * 2)
			{
				int current = sideCounter * 2 + 2;
				int next = sideCounter * 2 + 4;

				triangles[ i++ ] = current;
				triangles[ i++ ] = next;
				triangles[ i++ ] = next + 1;

				triangles[ i++ ] = current;
				triangles[ i++ ] = next + 1;
				triangles[ i++ ] = current + 1;

				sideCounter++;
			}

			// Sides (out)
			while( sideCounter < nbSides * 3 )
			{
				int current = sideCounter * 2 + 4;
				int next = sideCounter * 2 + 6;

				triangles[ i++ ] = current;
				triangles[ i++ ] = next;
				triangles[ i++ ] = next + 1;

				triangles[ i++ ] = current;
				triangles[ i++ ] = next + 1;
				triangles[ i++ ] = current + 1;

				sideCounter++;
			}


			// Sides (in)
			while( sideCounter < nbSides * 4 )
			{
				int current = sideCounter * 2 + 6;
				int next = sideCounter * 2 + 8;

				triangles[ i++ ] = next + 1;
				triangles[ i++ ] = next;
				triangles[ i++ ] = current;

				triangles[ i++ ] = current + 1;
				triangles[ i++ ] = next + 1;
				triangles[ i++ ] = current;

				sideCounter++;
			}
			#endregion

			//Conversion 16.08.16 by Dongyeop
			List<Vector3>verticesList = new List<Vector3>();
			verticesList.AddRange(vertices);

			List<Vector3>normalsList = new List<Vector3>();
			normalsList.AddRange(normales);

			List<Vector2>uvList = new List<Vector2>();
			uvList.AddRange(uvs);

			List<int> triangleList = new List<int>();
			triangleList.AddRange(triangles);

			return new MeshDraft
			{
				vertices = verticesList,
				normals = normalsList,
				uv = uvList,
				triangles = triangleList,
				name = "WatchCase"
			};
		}
		#endregion

		#region Indicator
		public static MeshDraft Indicator(Vector3 vertex0, Vector3 vertex1, Vector3 vertex2) {
			var face1 = MeshDraft.Triangle (vertex0, vertex1, vertex2);
			var face2 = MeshDraft.Triangle (vertex0, vertex2, vertex1);
			face1.Add (face2);
			return face1;
		}
		#endregion

		#region Watch Case2
		public static MeshDraft WatchCase2(float height, int nbSides, float bottomOutRadius, float bottomInRadius, float topOutRadius, float topInRadius)
		{
			int nbVerticesCap = nbSides * 2 + 2;
			int nbVerticesSides = nbSides * 2 + 2;

			#region Vertices

			// bottom + top + sides
			Vector3[] vertices = new Vector3[nbVerticesCap * 2 + nbVerticesSides * 2];
			int vert = 0;
			float _2pi = Mathf.PI * 2f;

			// Bottom cap
			int sideCounter = 0;
			while( vert < nbVerticesCap )
			{
				sideCounter = sideCounter == nbSides ? 0 : sideCounter;

				float r1 = (float)(sideCounter++) / nbSides * _2pi;
				float cos = Mathf.Cos(r1);
				float sin = Mathf.Sin(r1);
				vertices[vert] = new Vector3( cos * (bottomInRadius), -height/2, sin * (bottomInRadius));
				vertices[vert+1] = new Vector3( cos * (bottomOutRadius), -height/2, sin * (bottomOutRadius));
				vert += 2;
			}

			// Top cap
			sideCounter = 0;
			while( vert < nbVerticesCap * 2 )
			{
				sideCounter = sideCounter == nbSides ? 0 : sideCounter;

				float r1 = (float)(sideCounter++) / nbSides * _2pi;
				float cos = Mathf.Cos(r1);
				float sin = Mathf.Sin(r1);
				vertices[vert] = new Vector3( cos * (topInRadius), height/2, sin * (topInRadius));
				vertices[vert+1] = new Vector3( cos * (topOutRadius), height/2, sin * (topOutRadius));
				vert += 2;
			}

			// Sides (out)
			sideCounter = 0;
			while (vert < nbVerticesCap * 2 + nbVerticesSides )
			{
				sideCounter = sideCounter == nbSides ? 0 : sideCounter;

				float r1 = (float)(sideCounter++) / nbSides * _2pi;
				float cos = Mathf.Cos(r1);
				float sin = Mathf.Sin(r1);

				vertices[vert] = new Vector3(cos * (topOutRadius), height/2, sin * (topOutRadius));
				vertices[vert + 1] = new Vector3(cos * (bottomOutRadius), -height/2, sin * (bottomOutRadius));
				vert+=2;
			}

			// Sides (in)
			sideCounter = 0;
			while (vert < vertices.Length )
			{
				sideCounter = sideCounter == nbSides ? 0 : sideCounter;

				float r1 = (float)(sideCounter++) / nbSides * _2pi;
				float cos = Mathf.Cos(r1);
				float sin = Mathf.Sin(r1);

				vertices[vert] = new Vector3(cos * (topInRadius), height/2, sin * (topInRadius));
				vertices[vert + 1] = new Vector3(cos * (bottomInRadius), -height/2, sin * (bottomInRadius));
				vert += 2;
			}
			#endregion

			#region Normales

			// bottom + top + sides
			Vector3[] normales = new Vector3[vertices.Length];
			vert = 0;

			// Bottom cap
			while( vert < nbVerticesCap )
			{
				normales[vert++] = Vector3.down;
			}

			// Top cap
			while( vert < nbVerticesCap * 2 )
			{
				normales[vert++] = Vector3.up;
			}

			// Sides (out)
			sideCounter = 0;
			while (vert < nbVerticesCap * 2 + nbVerticesSides )
			{
				sideCounter = sideCounter == nbSides ? 0 : sideCounter;

				float r1 = (float)(sideCounter++) / nbSides * _2pi;

				normales[vert] = new Vector3(Mathf.Cos(r1), 0f, Mathf.Sin(r1));
				normales[vert+1] = normales[vert];
				vert+=2;
			}

			// Sides (in)
			sideCounter = 0;
			while (vert < vertices.Length )
			{
				sideCounter = sideCounter == nbSides ? 0 : sideCounter;

				float r1 = (float)(sideCounter++) / nbSides * _2pi;

				normales[vert] = -(new Vector3(Mathf.Cos(r1), 0f, Mathf.Sin(r1)));
				normales[vert+1] = normales[vert];
				vert+=2;
			}
			#endregion

			#region UVs
			Vector2[] uvs = new Vector2[vertices.Length];

			vert = 0;
			// Bottom cap
			sideCounter = 0;
			while( vert < nbVerticesCap )
			{
				float t = (float)(sideCounter++) / nbSides;
				uvs[ vert++ ] = new Vector2( 0f, t );
				uvs[ vert++ ] = new Vector2( 1f, t );
			}

			// Top cap
			sideCounter = 0;
			while( vert < nbVerticesCap * 2 )
			{
				float t = (float)(sideCounter++) / nbSides;
				uvs[ vert++ ] = new Vector2( 0f, t );
				uvs[ vert++ ] = new Vector2( 1f, t );
			}

			// Sides (out)
			sideCounter = 0;
			while (vert < nbVerticesCap * 2 + nbVerticesSides )
			{
				float t = (float)(sideCounter++) / nbSides;
				uvs[ vert++ ] = new Vector2( t, 0f );
				uvs[ vert++ ] = new Vector2( t, 1f );
			}

			// Sides (in)
			sideCounter = 0;
			while (vert < vertices.Length )
			{
				float t = (float)(sideCounter++) / nbSides;
				uvs[ vert++ ] = new Vector2( t, 0f );
				uvs[ vert++ ] = new Vector2( t, 1f );
			}
			#endregion

			#region Triangles
			int nbFace = nbSides * 4;
			int nbTriangles = nbFace * 2;
			int nbIndexes = nbTriangles * 3;
			int[] triangles = new int[nbIndexes];

			// Bottom cap
			int i = 0;
			sideCounter = 0;
			while (sideCounter < nbSides)
			{
				int current = sideCounter * 2;
				int next = sideCounter * 2 + 2;

				triangles[ i++ ] = next + 1;
				triangles[ i++ ] = next;
				triangles[ i++ ] = current;

				triangles[ i++ ] = current + 1;
				triangles[ i++ ] = next + 1;
				triangles[ i++ ] = current;

				sideCounter++;
			}

			// Top cap
			while (sideCounter < nbSides * 2)
			{
				int current = sideCounter * 2 + 2;
				int next = sideCounter * 2 + 4;

				triangles[ i++ ] = current;
				triangles[ i++ ] = next;
				triangles[ i++ ] = next + 1;

				triangles[ i++ ] = current;
				triangles[ i++ ] = next + 1;
				triangles[ i++ ] = current + 1;

				sideCounter++;
			}

			// Sides (out)
			while( sideCounter < nbSides * 3 )
			{
				int current = sideCounter * 2 + 4;
				int next = sideCounter * 2 + 6;

				triangles[ i++ ] = current;
				triangles[ i++ ] = next;
				triangles[ i++ ] = next + 1;

				triangles[ i++ ] = current;
				triangles[ i++ ] = next + 1;
				triangles[ i++ ] = current + 1;

				sideCounter++;
			}


			// Sides (in)
			while( sideCounter < nbSides * 4 )
			{
				int current = sideCounter * 2 + 6;
				int next = sideCounter * 2 + 8;

				triangles[ i++ ] = next + 1;
				triangles[ i++ ] = next;
				triangles[ i++ ] = current;

				triangles[ i++ ] = current + 1;
				triangles[ i++ ] = next + 1;
				triangles[ i++ ] = current;

				sideCounter++;
			}
			#endregion

			//Conversion 16.08.16 by Dongyeop
			List<Vector3>verticesList = new List<Vector3>();
			verticesList.AddRange(vertices);

			List<Vector3>normalsList = new List<Vector3>();
			normalsList.AddRange(normales);

			List<Vector2>uvList = new List<Vector2>();
			uvList.AddRange(uvs);

			List<int> triangleList = new List<int>();
			triangleList.AddRange(triangles);

			return new MeshDraft
			{
				vertices = verticesList,
				normals = normalsList,
				uv = uvList,
				triangles = triangleList,
				name = "WatchCase"
			};
		}
		#endregion

		public static MeshDraft OffSet(MeshDraft target, Vector3 offset) {
			target.Move (offset);
			return target;
		}
	}
}

