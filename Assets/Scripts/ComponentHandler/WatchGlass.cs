﻿using UnityEngine;
using System.Collections;
using ProceduralToolkit;
using AssemblyCSharp;

public class WatchGlass : WatchBehaviour {
	public WatchCase case_;
	public WatchBezel bezel;

	private float radius;
	public float Radius {
		get { return radius; }
		set { radius = value; }
	}

	private float height = 0.02f;
	public float Height {
		get { return height; }
		set { height = value; }
	}

	public int segments { get; set; }

	// Use this for initialization
	void Start () {
		filter = GetComponent<MeshFilter> ();

		radius = bezel.TopInRadius;
		segments = 60;


		Generate ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void Generate() {
		MeshDraft draft = MeshDraft.Cylinder (radius, segments, height);
		draft = MeshDraft.OffSet (draft, new Vector3 (0, case_.Height / 2 + height/2, 0));

		filter.mesh = draft.ToMesh ();
	}

	public override void ReflectUpdate() {
		Dispose ();
		Generate ();
	}
}
