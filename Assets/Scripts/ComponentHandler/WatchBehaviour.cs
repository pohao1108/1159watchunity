﻿using System;
using UnityEngine;

namespace AssemblyCSharp
{
	public class WatchBehaviour: MonoBehaviour
	{
		public MeshFilter filter;

		protected IndicatorBehaviour[] indicatorHandlers;

		public bool Active { get; set; }

		/**
		 * scaling indicator
		 * value changed in UpdateDimension and UpdateIndicators
		 **/
		public bool scaling = false;

		public virtual void UpdateDimension(float scale) {
			Debug.Log (scale);
		}

		public virtual void ReflectUpdate() {
			Debug.Log ("ReflectUpdate not implemented yet");
		}

		public virtual void UpdateIndicators() {
			scaling = false;
			foreach (IndicatorBehaviour ib in indicatorHandlers) {
				ib.ReflectUpdate ();
			}
		}

		public void Dispose() {
			filter.mesh.Clear ();
			Destroy (filter.GetComponent<MeshCollider>());
		}
	}
}

