﻿using UnityEngine;
using System.Collections;

public class MainMenuBtn : MonoBehaviour {

	public GameObject mMainMenuClose;
	public GameObject mMainMenuOpen;
	public GameObject mWatch;
	public GameObject mColorPanel;
	public GameObject mGrayScreen;
	public GameObject mTouchScript;
	public bool active;

	// Use this for initialization
	void Start () {
		active = false;
	}

	// Enable Main Panel 06.09.16 Dongyeop
	public void EnableBoolAnimator (Animator anim)
	{
		active = true;
		anim.SetBool("SlideIn", true);
		anim.SetBool("SlideOut", false);
		mMainMenuOpen.SetActive(false);
		mMainMenuClose.SetActive(true);
		//mTouchScript.SetActive(false);

		// Closing button removed as swipe left to close the menu 05.10.16 Dongyeop
		//mMainMenuClose.SetActive(true);
		//mWatch.SetActive(false);

		//Disable colour panel when menu is on
		mColorPanel.SetActive(false);
		// Turn on gray screen to cover watch model
		mGrayScreen.SetActive(true);
	}

	// Disable Main Panel 06.09.16 Dongyeop
	public void DisableBoolAnimator (Animator anim)
	{
		active = false;
		anim.SetBool("SlideOut", true);
		anim.SetBool("SlideIn", false);
		mMainMenuClose.SetActive(false);
		mMainMenuOpen.SetActive(true);
		mWatch.SetActive(true);
		mGrayScreen.SetActive(false);
		mColorPanel.SetActive(true);
		//mTouchScript.SetActive(true);

			//spriteRenderer.sprite = mainMenuOff;
		
	}
}
