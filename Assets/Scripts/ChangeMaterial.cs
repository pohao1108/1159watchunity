﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class ChangeMaterial : MonoBehaviour {

	// Default renderer may not need. 12.09.16 Dongyeop
	// As this is public, we can set the size of the array on the interface.
	public Material[] material;
	public GameObject cameraController;

    public Renderer watchCase;
    public Renderer watchBezel;
    public Renderer watchLug;

    // Default renderer 
    Renderer rend;
	Renderer selectedRenderer;

	ClickAction actionScript;

    private bool applyToAll;
    bool changed;

    // Splash Image
    public Image mSplashImage;
    public float flashSpeed = 115f;
    public Color flashColour = new Color(1f, 0f, 0f, 0.1f);

    // Use this for initialization
    void Start () {

		//Access to the game object renderer.
		rend = GetComponent<Renderer>();
		rend.enabled = true;

		//Set the first material from the array.
		rend.sharedMaterial = material[0];

		// Assign renderer from ClickAction (Touchinput)
		actionScript = cameraController.GetComponent<ClickAction>();
		// Assign default material as well in the touch event to avoid "null" 24.10.16 Dongyeop
		actionScript.mOriginalMaterial = material[0];
        applyToAll = true;

	}

	void Update() {
		//Update to the game object renderer.
		selectedRenderer = actionScript.getRenderer();
		if (selectedRenderer != null)
        {
            selectedRenderer.enabled = true;
            applyToAll = false;
        }

   		/*
        //Update Flash image once changed   
        if(changed)
        {
        	Flash();
            mSplashImage.color = flashColour;
        }
        else
        {
        	Flash();
            mSplashImage.color = Color.Lerp(mSplashImage.color, Color.clear, flashSpeed * Time.deltaTime);
        }
        // Change back to false
        changed = false;
        */
    }

	// Called in the color Panel UI buttons by Dongyeop 10.09.16
	// Chnage the material
	public void ChangeColorGoldGlossy()
	{
        // Change all when nothing selected 19.10.16 Dongyeop
        if (applyToAll == true)
        {
            watchCase.GetComponent<Renderer>().sharedMaterial = material[1];
            watchBezel.GetComponent<Renderer>().sharedMaterial = material[1];
            watchLug.GetComponent<Renderer>().sharedMaterial = material[1];
            Debug.Log("Changed All");
        }
        else if (applyToAll == false && selectedRenderer != null)
        {
            // Change only selected component 19.10.16 Dongyeop
            selectedRenderer.sharedMaterial = material[1];
            Flash();
            actionScript.mOriginalMaterial = material[1];
            Debug.Log("Final Selected" + selectedRenderer);
        }
        changed = true;
	}

	// Chnage the material
	public void ChangeColorGoldMatt()
	{
        // Change all when nothing selected 19.10.16 Dongyeop
        if (applyToAll == true)
        {
            watchCase.GetComponent<Renderer>().sharedMaterial = material[2];
            watchBezel.GetComponent<Renderer>().sharedMaterial = material[2];
            watchLug.GetComponent<Renderer>().sharedMaterial = material[2];
            Debug.Log("Changed All");
        }
        else if (applyToAll == false && selectedRenderer != null)
        {
            selectedRenderer.sharedMaterial = material[2];
			actionScript.mOriginalMaterial = material[2];
        }
        changed = true;
    }

	// Chnage the material
	public void ChangeColorSilverGlossy()
	{
        // Change all when nothing selected 19.10.16 Dongyeop
        if (applyToAll == true)
        {
            watchCase.GetComponent<Renderer>().sharedMaterial = material[3];
            watchBezel.GetComponent<Renderer>().sharedMaterial = material[3];
            watchLug.GetComponent<Renderer>().sharedMaterial = material[3];
            Debug.Log("Changed All");
        }
        else if (applyToAll == false && selectedRenderer != null)
        {
            selectedRenderer.sharedMaterial = material[3];
			actionScript.mOriginalMaterial = material[3];
        }
        changed = true;
    }

	// Chnage the material
	public void ChangeColorSilverMatt()
	{
        // Change all when nothing selected 19.10.16 Dongyeop
        if (applyToAll == true)
        {
            watchCase.GetComponent<Renderer>().sharedMaterial = material[4];
            watchBezel.GetComponent<Renderer>().sharedMaterial = material[4];
            watchLug.GetComponent<Renderer>().sharedMaterial = material[4];
            Debug.Log("Changed All");
        }
        else if (applyToAll == false && selectedRenderer != null)
        {
            selectedRenderer.sharedMaterial = material[4];
			actionScript.mOriginalMaterial = material[4];
        }
        changed = true;
    }

	public void ChangeColorBlackGlossy()
	{
        // Change all when nothing selected 19.10.16 Dongyeop
        if (applyToAll == true)
        {
            watchCase.GetComponent<Renderer>().sharedMaterial = material[5];
            watchBezel.GetComponent<Renderer>().sharedMaterial = material[5];
            watchLug.GetComponent<Renderer>().sharedMaterial = material[5];
            Debug.Log("Changed All");
        }
        else if (applyToAll == false && selectedRenderer != null)
        {
            selectedRenderer.sharedMaterial = material[5];
			actionScript.mOriginalMaterial = material[5];
        }
        changed = true;
    }

	// Chnage the material
	public void ChangeColorBlackMatt()
	{
        // Change all when nothing selected 19.10.16 Dongyeop
        if (applyToAll == true)
        {
            watchCase.GetComponent<Renderer>().sharedMaterial = material[6];
            watchBezel.GetComponent<Renderer>().sharedMaterial = material[6];
            watchLug.GetComponent<Renderer>().sharedMaterial = material[6];
            Debug.Log("Changed All");
        }
        else if (applyToAll == false && selectedRenderer != null)
        {
            selectedRenderer.sharedMaterial = material[6];
			actionScript.mOriginalMaterial = material[6];
        }
        changed = true;
    }

	IEnumerator Flash() {
		selectedRenderer.sharedMaterial = material[6];
		yield return new WaitForSeconds(.5f);
		selectedRenderer.sharedMaterial = material[1];
		Debug.Log ("Flashed");

    }
}
